#!/usr/bin/env python

import subprocess
import config

for ip in config.borne_ip.values():
	subprocess.Popen(["scp", "-i", config.key_path, "-o", "StrictHostKeyChecking=no", "mastermind.sh", "root@%s:" % ip])
