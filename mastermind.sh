#!/bin/sh


r=$1

if [ $r = 0 ]; then         
echo none > /sys/class/leds/ubnt:orange:dome/trigger
echo none > /sys/class/leds/ubnt:green:dome/trigger
fi

if [ $r = 1 ]; then         
echo default-on > /sys/class/leds/ubnt:orange:dome/trigger
echo none > /sys/class/leds/ubnt:green:dome/trigger
fi

if [ $r = 2 ]; then         
echo none > /sys/class/leds/ubnt:orange:dome/trigger
echo default-on > /sys/class/leds/ubnt:green:dome/trigger
fi

