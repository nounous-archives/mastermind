#!/usr/bin/env python
import Tkinter as tk
import tkMessageBox
import random
import paramiko
import config
from PIL import Image, ImageTk

## Gplv3
## Raphael David Lasseri, Michael Paulon
## Script principal du jeu de mastermind

master = tk.Tk()

key = paramiko.RSAKey.from_private_key_file(config.key_path)

debug=config.debug_mode
liste_borne = []
choix = []
menu_choix = []
atrouver = []
couleurs=["Rouge","Jaune","Bleu","Noir","Blanc"]

im = Image.open('logo.png')
tkimage = ImageTk.PhotoImage(im)
myvar=tk.Label(master,image = tkimage)
myvar.place(x=0, y=0, relwidth=1, relheight=1)

# Cree le menu de commande et calcule la combinaison a trouver
for i,valeur in enumerate(config.borne_ip):
	liste_borne.append(paramiko.SSHClient())
	liste_borne[-1].set_missing_host_key_policy(paramiko.AutoAddPolicy())
	liste_borne[-1].connect(username="root",hostname=config.borne_ip[valeur],pkey=key)
        choix.append(tk.StringVar(master))
	choix[-1].set("Rouge")
	menu_choix.append(tk.OptionMenu(master,choix[-1],"Rouge","Jaune","Bleu","Noir","Blanc"))
	menu_choix[-1].place(relx=float((i+1))/10,rely=.1)
	atrouver.append(couleurs[random.randrange(0,4)])
        if debug==True:
            print "connexion established",valeur   
if debug==True:
        print "Done"
	print atrouver

def compare(liste1,liste2):
    """ Compare le resultat de l'user et la combinaison a trouver"""
    resultat=[0,0,0,0]
    for i in range(len(liste1)) :
        if liste1[i] in liste2:
            resultat[i]=1
	if liste1[i]==liste2[i] :
	    resultat[i]=2
    return resultat

def ok():
    """ Boucle principale, a chaque essaie on compare et on informe avec les diodes"""
    if ok.count<=config.max_try :
        affichage=[]
        for i,pos in enumerate(choix) :
            affichage.append(tk.Message(master, text="%s" % choix[i].get(), width = 50))
            affichage[-1].place(relx = float((i+1))/10, rely = .2+(ok.count)*.05)

    if ok.count>config.max_try :
       master.quit()

    counter(ok)
    if debug == True :
        compare([choix[i].get() for i in range(len(choix))],atrouver)
        print ok.count
    for k,borne in enumerate(liste_borne):
        borne.exec_command("./mastermind.sh %i" % compare([choix[i].get() for i in range(len(choix))],atrouver)[k])

    if compare([choix[i].get() for i in range(len(choix))],atrouver) == [2]*len(compare([choix[i].get() for i in range(len(choix))],atrouver)):
        if debug == True :
            print('gagne')
        tkMessageBox.showinfo("Winner !!!", "Bravo !!!")
        master.quit()
        quit()


button = tk.Button(master, text="Valider", command=ok)
button.place(relx=.8, rely=.1)

def counter(ok):
   ok.count+=1
ok.count=0
master.quit()

def quit():
    global master
    master.destroy()

master.mainloop()







