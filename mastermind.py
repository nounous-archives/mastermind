#!/usr/bin/env python

import paramiko
import random
import config

key = paramiko.RSAKey.from_private_key_file(config.key_path)

liste_bornes=[]

for valeur in config.borne_ip:
    liste_bornes.append(paramiko.SSHClient())
    liste_bornes[-1].set_missing_host_key_policy(paramiko.AutoAddPolicy())
    liste_bornes[-1].connect(username="root",hostname=config.borne_ip[valeur],pkey=key)
    if config.debug_mode==True:
        print "connexion established", valeur
while True:
    for borne in liste_bornes:
        borne.exec_command("./mastermind.sh %i" % random.randrange(0,3))
        random.seed()

